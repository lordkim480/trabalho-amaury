package com.example.projeto.Trab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);

        final EditText editTextUser = (EditText) findViewById(R.id.campoUsuario);
        editTextUser.setText(PreferenciasUser.getValuesString(getContext(),"nameUser"));

        if(!PreferenciasUser.getValuesBoolean(this.getContext(),"manterConectado")) {
            Button button = (Button) findViewById(R.id.buttonEntrar);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String user = editTextUser.getText().toString();

                    EditText editTextSenha = (EditText) findViewById(R.id.campoSenha);
                    String senha = editTextSenha.getText().toString();

                    if (user.equals("teste") && senha.equals("123")) {
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        Bundle params = new Bundle();
                        params.putString("nome", user);
                        intent.putExtras(params);


                        PreferenciasUser.setValuesString(getContext(), "nameUser", user);

                        if (checkBox.isChecked()) {
                            PreferenciasUser.setValuesBoolean(getContext(), "manterConectado", true);
                        } else {
                            PreferenciasUser.setValuesBoolean(getContext(), "manterConectado", false);
                        }

                        startActivity(intent);
                    } else {
                        alert("Falha ao efetuar login");
                    }

                }
            });
        }
        else {
            Intent intent = new Intent(getContext(), MainActivity.class);
            startActivity(intent);

        }
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getContext() {
        return this;
    }
}
