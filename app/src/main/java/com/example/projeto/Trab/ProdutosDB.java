package com.example.projeto.Trab;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ProdutosDB extends SQLiteOpenHelper {

    //nome do banco
    public static final String NOME_BANCO = "produtos";
    private static final int VERSAO_BANCO = 1;
    private static final String TAG = "sql";

    public ProdutosDB(Context context){
        //context, nome do banco, factory, versao
        super(context,NOME_BANCO,null,VERSAO_BANCO);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        Log.d(TAG, "Criando a tabela Produto...");
        db.execSQL("create table if not exists produto(_id integer primary key autoincrement," +
                "nome_produto text, qtde text);");
        Log.d(TAG, "Tabela Produto criada com sucesso");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        //Caso mude a versao do banco de dados, podemos executar um SQL aqui
    }

    //Insere um novo produto, ou atualiza se ja existe
    public long save(Produto produto){
        long id = produto.getId();
        SQLiteDatabase db = getWritableDatabase();
        Log.d(TAG, "Produto criado com sucesso");
        try{
            ContentValues values = new ContentValues();
            if( produto.getId() != 0 ){
                values.put("_id",produto.getId());
            }
            values.put("nome_produto",produto.getNome());
            values.put("qtde",produto.getQtde());
            if(id!=0){
                String _id = String.valueOf(produto.getId());
                String[] whereArgs = new String[]{_id};
                //update usuario set values= ... where _id=?
                int count = db.update("produto",values,"_id=?",whereArgs);
                return count;
            }
            else{
                //insert into usuario values (...)
                id = db.insert("produto","",values);
                return id;
            }
        }finally {
            db.close();
        }


    }

    //delete o produto
    public int delete(Produto produto){
        SQLiteDatabase db = getWritableDatabase();
        try{
            //delete from usuario where _id=?
            int count = db.delete("produto", "_id=?", new String[]{String.valueOf(produto.id)});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }
        finally {
            db.close();
        }
    }

    //Consulta a lista com todos os produtos
    public List<Produto> findAll(){
        SQLiteDatabase db = getWritableDatabase();
        List<Produto> produtos = new ArrayList<>();
        try{
            //select *from usuario
            Cursor c = db.query("produto", null, null, null, null, null, null, null);
            while (c.moveToNext()){
                int id = c.getInt(c.getColumnIndex("_id"));
                String nome = c.getString(c.getColumnIndex("nome_produto"));
                String qtde = c.getString(c.getColumnIndex("qtde"));
                produtos.add( new Produto(id,nome,qtde));
            }
            return produtos;
        }
        finally {
            db.close();
        }
    }

    //Consulta o produto pelo nome
    public List<Produto> findById(int id){
        SQLiteDatabase db = getWritableDatabase();
        try{
            //select *from usuario where nome=?
            Cursor c = db.query("produto", null, "_id=" +id , null, null, null, null, null);
            return toList(c);
        }
        finally {
            db.close();
        }
    }

    //Lê o cursor e cria a lista de usuarios
    private List<Produto> toList(Cursor c){
        List<Produto> produtos = new ArrayList<Produto>();

        if(c.moveToFirst()){
            do{
                Produto p = new Produto();
                produtos.add(p);
                //recupera os atributos de usuario
                p.id = c.getInt(c.getColumnIndex("_id"));
                p.nome = c.getString(c.getColumnIndex("nome_produto"));
                p.qtde = c.getString(c.getColumnIndex("qtde"));
            }while(c.moveToNext());
        }
        return produtos;
    }
    //Executa um SQL
    public void execSQL(String sql){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL(sql);
        }
        finally {
            db.close();
        }
    }
    //Executa um SQL
    public void execSQL(String sql,Object[] args){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL(sql,args);
        }
        finally {
            db.close();
        }
    }
}
