package com.example.projeto.Trab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class EditarActivity extends AppCompatActivity {

    ProdutosDB pDB = new ProdutosDB(this);
    EditText nomeText,qtdeText;
    String nome,qtde;
    int id_produto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);
        Button buttonAlterar = (Button) findViewById(R.id.botaoEditar);
        Button buttonDeletar = (Button) findViewById(R.id.botaoExcluir);

        nomeText = (EditText) findViewById(R.id.nome_produto);
        qtdeText = (EditText) findViewById(R.id.qtde);

        id_produto = this.getIntent().getIntExtra("id_produto",0);

        final Produto produto = pDB.findById(id_produto).get(0);
        nomeText.setText(produto.getNome());
        qtdeText.setText(produto.getQtde());

        buttonAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nome = nomeText.getText().toString();
                qtde = qtdeText.getText().toString();

                Produto produto1 = new Produto(id_produto,nome,qtde);
                pDB.save(produto1);

                alert("Produto alterado!");
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        buttonDeletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nome = nomeText.getText().toString();
                qtde = qtdeText.getText().toString();

                Produto produto1 = new Produto(id_produto,nome,qtde);

                pDB.delete(produto1);

                alert("Produto Deletado!");
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getContext() {
        return this;
    }
}
